from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World! There is more to this Web app than meets the eye. http://heroku.brie.ninja/(about|hello|home)'

@app.route('/hello')
def hello():
    return 'Hi. Welcome to the **hello** page.'

@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/about')
def all_about():
    return render_template('about.html')