def test_heroku_brie_ninja(host):
    heroku_brie_ninja = host.addr("heroku.brie.ninja")
    assert heroku_brie_ninja.is_resolvable is True

def test_devnull_brie_ninja(host):
    devnull_brie_ninja = host.addr("devnull.brie.ninja")
    assert devnull_brie_ninja.is_resolvable is False    # Change to True to break the tests and stop the application from being deployed.
