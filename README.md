# A basic Flask App

## Production
[View in Heroku](https://lit-anchorage-62696.herokuapp.com/)

## Development
View the app locally:
  - `gunicorn app:app -b 0.0.0.0:5000`
  - `heroku local web`

## Heroku Details

```
heroku create                                                   130 ↵
Creating app... done, ⬢ lit-anchorage-62696
https://lit-anchorage-62696.herokuapp.com/ | https://git.heroku.com/lit-anchorage-62696.git
```


## Links
The [Building a website with Python Flask](https://pythonhow.com/building-a-website-with-python-flask/) tutorial from `pythonhow.com` was super helpful.

Test and deploy Python application to Heroku using GitLab CI - [docs](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html) - I found this doc way too late.

---
## Run with virtualenv
- Create a virtualenv folder `virtualenv -p python3 venv`
- Activate `source venv/bin/activate`
- Install the requirements `pip install -r requirements.txt`
- Run gunicorn `gunicorn app:app --bind 0.0.0.0:5000 --reload`

## Run with Docker
- Build the image with `docker build -t hello-app .`
- Run the container with `docker run -d -p 5000:5000 -e PORT=5000 --name hello-server hello-app`
- Check that the container is running
- Go to `localhost:5000`. You should see `Hello, World!`
